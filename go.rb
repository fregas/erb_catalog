
require 'erb'
require 'csv'

class CsvData
  attr_accessor :items

  def initialize(items)
    @items = items
  end    

  def get_binding
    binding()
  end

  def self.get_urls(item) 
    if item["Image"] == nil 
      return []
    end
    
    item["Image"].split(",").map{|num| "photos/IMG_#{num}.JPG"}
  end
end

csv = CSV.parse(File.read("data.csv"), headers: true)

csv = csv.reject {|item| item["Sold To"] != nil }



data = CsvData.new(csv)


template = ERB.new(File.read("index.html.erb"))

File.write("sale/index.html", template.result( data.get_binding() ) )


